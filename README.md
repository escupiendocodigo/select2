Alguien en mi trabajo me comentó que tenia algunos problemas al cargar un combo con la libreria select2 así que para ayudarle me puse manos ala obra y realice este ejemplo para futuras referencias espero les sirva de ayuda.

# Select2

Este repositorio te ayuda a realizar la carga de un select utilizando la libreria Select2

## Clonatelo 

* `git clone <repositorio-a-clonar-url>` Url de este repositorio
* `abrir IndexSelect2.html`

## Ejecutalo

* Una vez descargado, tienes que abrirlo con  tu navegador preferido. Ejemplo: Chrome


## Canal de Youtube
* [EscupiendCódigo](https://youtube.com/channel/UC4w8VIeA7H8xjrASPTuPMxA)

## Apoyame
* Si te gusta el contenido puedes apoyarme invitandome un pastelito
* [ApoyoAlCreador](https://www.paypal.com/paypalme/byjazr)

